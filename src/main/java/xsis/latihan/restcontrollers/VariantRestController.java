package xsis.latihan.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xsis.latihan.service.VariantService;

@RestController
@RequestMapping(path="/api/variant", produces="application/json")
@CrossOrigin(origins="*")
public class VariantRestController {

	@Autowired
	private VariantService variantService;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllVariant() {
		return new ResponseEntity<>(variantService.findAllVariant(), HttpStatus.OK);
	}
}
