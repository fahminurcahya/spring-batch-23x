package xsis.latihan.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.service.CategoryService;

@RestController
@RequestMapping(path="/api/category", produces="application/json")
@CrossOrigin(origins="*")
public class CategoryRestController {

		@Autowired
		private CategoryService categoryService;
		
		@GetMapping("/")
		public ResponseEntity<?> findAllCategory() {
			return new ResponseEntity<>(categoryService.findAllCategory(), HttpStatus.OK);
		}
		
		@PostMapping("/add")
		public  ResponseEntity<?> saveCategory(@RequestBody CategoryModel category){
			return new ResponseEntity<>(categoryService.save(category),HttpStatus.OK);
		}
		
		@PutMapping("/put")
		public  ResponseEntity<?> putCategory(@RequestBody CategoryModel category){
			return new ResponseEntity<>(categoryService.save(category),HttpStatus.OK);
		}
		@DeleteMapping("/delete/{id}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public void deleteCategory(@PathVariable("id") Long Id){
			try {
				categoryService.delete(Id);
			} catch (EmptyResultDataAccessException e) {
				// TODO: handle exception
			}
		}
		
		
}
