package xsis.latihan.service;

import java.util.List;

import xsis.latihan.models.ProductModel;

public interface ProductService {

	List<ProductModel> findAllProduct();
	
	ProductModel save(ProductModel product);
	void delete(Long Id);
}
