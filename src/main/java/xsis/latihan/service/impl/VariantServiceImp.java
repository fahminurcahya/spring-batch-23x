package xsis.latihan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xsis.latihan.models.VariantModel;
import xsis.latihan.repositories.VariantRepo;
import xsis.latihan.service.VariantService;

@Service
public class VariantServiceImp implements VariantService {

	@Autowired
	private VariantRepo variantRepository;
	
	@Override
	public List<VariantModel> findAllVariant() {
		// TODO Auto-generated method stub
		return variantRepository.findAll();
	}
}
