package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.repositories.CategoryRepo;

@Controller
@RequestMapping("/category/")
public class Category {
	
	@Autowired
	private CategoryRepo categoryrepo;
	
	@GetMapping("index")
	public  ModelAndView index() {
		ModelAndView view = new ModelAndView("/category/index");
		List<CategoryModel> category = this.categoryrepo.findAll();
		view.addObject("category", category);
		return view;
	}
	@GetMapping("form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/category/form");
		CategoryModel categorymodel = new CategoryModel();
		view.addObject("category", categorymodel);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute CategoryModel categorymodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:../index/");
		} else {
			this.categoryrepo.save(categorymodel);
			return new ModelAndView("redirect:../index/");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/category/form");
		CategoryModel categorymodel=this.categoryrepo.findById(id).orElse(null);
		view.addObject("category", categorymodel);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/category/delete");
		CategoryModel categorymodel=this.categoryrepo.findById(id).orElse(null);
		view.addObject("category", categorymodel);
		return view;
	}
	
	@PostMapping("remove")
	public ModelAndView remove(@ModelAttribute CategoryModel categorymodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:../index/");
		} else {
			this.categoryrepo.delete(categorymodel);
			return new ModelAndView("redirect:../index/");
		}
	}

}
