package xsis.latihan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import xsis.latihan.models.VariantModel;

public interface VariantRepo  extends JpaRepository<VariantModel, Long> {

}
