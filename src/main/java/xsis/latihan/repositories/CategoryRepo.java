package xsis.latihan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import xsis.latihan.models.CategoryModel;


public interface CategoryRepo  extends JpaRepository<CategoryModel, Long> {

}
