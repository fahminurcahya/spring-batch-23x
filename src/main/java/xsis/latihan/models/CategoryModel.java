package xsis.latihan.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="kategori")
public class CategoryModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@Column(name="category_name")
	private String CategoryName;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getCategoryName() {
		return CategoryName;
	}

	public void setCategoryName(String categoryName) {
		CategoryName = categoryName;
	}
	
	public CategoryModel() {
		
	}
	public CategoryModel(String categoryName) {
		this.CategoryName = categoryName;
	}
	
	//one to many category -> variant
	@OneToMany(mappedBy="categorymodel", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	@JsonManagedReference
	@Column(nullable=true)
	private Set<VariantModel> variants;
	
	//one to many category -> product
	@OneToMany(mappedBy="categorymodel", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	@JsonManagedReference
	@Column(nullable=true)
	private Set<ProductModel> products;
	
	
		
}
